<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpcli' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1:8889' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'H bx.tcfZ33>s9:.+r&k_@QBE9gj5C8PG{O#-0. 6l$d8ZhKl!.KjYfCpg2ma=|G' );
define( 'SECURE_AUTH_KEY',   '}m <ED_saJ]iDXFrQYTAA=uABcg$EvKo2]XcZ-gm?<TnVijk_7x^A_RQB6VvP%(p' );
define( 'LOGGED_IN_KEY',     'A;_m+]Np+mD27rf!.D;<DL5|n%5BY(ZU 3`5hcBeh6+eh,h^`{ft#B4-?/z%R]@,' );
define( 'NONCE_KEY',         'J%Klk(5.?6aAxiT5.pfB/WFjB#veJ9}J8EO}Z|6SdJ{N9eeIr7TN0Msk7cOeVQ6X' );
define( 'AUTH_SALT',         '7sH_>bWOYiC^uygapUEjH^kh1Z.xEu]7_KObTkg`[]D,~wRkLW8wr+h:y_QLgHN+' );
define( 'SECURE_AUTH_SALT',  'M<}iDEvox#zZxU&dx2InOx3mW5baA^?&_6*FHAR)%a@6KqNXh*dmDYql#eP=;st%' );
define( 'LOGGED_IN_SALT',    'u04.PWa,)4z)v/huZ:H=8ZwZ1_>zL~/~h#AiQ0V*l3IExoy;r+s&[H*A2ud=pw|x' );
define( 'NONCE_SALT',        'r{Sc4`.(1}5;@!lnNVm1)Ptgtz&=)!8*(l@=f5(Q|FsI,)u?Uci$e2|h`01GaC<O' );
define( 'WP_CACHE_KEY_SALT', '40ei_6LsZ>GRNTAanlu%G!wbxLjz(x!oR2A$c]*@a8k<#=@DlHJts:vb^u:/sJ#y' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
